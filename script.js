//hw06 Задание 1. В блоке "базовый уровень" взять каждую строку (заголовок), преобразовать в верхний регистр.

document.querySelector('.level-base').querySelectorAll('.item-title').forEach(function (heading) {
  heading.innerText = heading.innerText.toUpperCase();
});

//hw06 Задание 2. В блоке "базовый уровень" взять каждую строку (описание), обрезать до 20 символов, и в конце вывести "...".

document.querySelector('.level-base').querySelectorAll('.item-description').forEach(function (description) {
  let sliced = description.innerHTML.slice(0, 20);
  if (sliced.length < description.innerHTML.length) {
    sliced += '...';
    description.innerHTML = sliced;
  }
});

//hw07 Написать функцию которая будет открывать и закрывать пункты меню по клику на корневой пункт меню .
//     Стрелочка меню должна смотреть вверх в открытом состоянии и смотреть вниз в закрытом состоянии.

document.querySelector('.js-first-menu').querySelectorAll('.js-first-menu-list').forEach(function (el_menu) {
  if (el_menu.querySelector('.second-menu')) {
    el_menu.onclick = function () {
      el_menu.querySelector('.arrow-link').classList.toggle('arrow-open');
      el_menu.querySelector('.second-menu').classList.toggle('menu-open');
    }
  }
});

/*hw08 Задание №1:
    1) Вам необходимо добавить дополнительный блок "Все лекции" (img_01)
    2) При загрузке страницы onload вам необходимо брать все лекции на сайте, складывать их в массив и вставлять в сверстанный вами блок "Все лекции". Фактически блок "Все лекции" должен состоять из всех лекций на сайте (img_02), список которого должен формироваться с помощью js.
    3) В кнопке "{{NUMBER}} лекций" необходимо выводить кол-во лекций в списке "все лекции". (img_03)*/

window.onload = function () {

  /*hw09 Необходимо написать функцию, которая возвращает объект со всеми лекциями и выводит результат в консоль */
  let obj = {};
  let obj_description = {
    title: "", // Заголовок лекции
    description: "", // Описание лекции
    date: "", // Дата лекции
    image: "", // Image лекции
    label: "", // Лейбл лекции
  };
  let leng = 0;
  let lectures = [];
  document.querySelectorAll('.js-item').forEach(function (item) {
    if (`${item.dataset.group}` in obj) {
      leng = obj[`${item.dataset.group}`].length; //записала длину массива в переменную leng
      obj[`${item.dataset.group}`][`${leng}`] = { ...obj_description };
      for (key in obj[`${item.dataset.group}`][`${leng}`]) {
        if (key == 'image') {
          obj[`${item.dataset.group}`][`${leng}`][`${key}`] = getUrlImg(item.querySelector(`.item-${key}`).src);
        } else {
          obj[`${item.dataset.group}`][`${leng}`][`${key}`] = item.querySelector(`.item-${key}`).innerHTML;
        }
      }
    } else {
      obj[`${item.dataset.group}`] = [{ ...obj_description }];
      for (key in obj[`${item.dataset.group}`][0]) {
        if (key == 'image') {
          obj[`${item.dataset.group}`][0][`${key}`] = getUrlImg(item.querySelector(`.item-${key}`).src);
        } else {
          obj[`${item.dataset.group}`][0][`${key}`] = item.querySelector(`.item-${key}`).innerHTML;
        }
      }
    }

    lectures.push(item);
    document.querySelector(".js-all-lectures-items").innerHTML += `<div class="js-item item" data-group="${item.dataset.group}">${item.innerHTML}</div>`;
  });

  console.log("obj", obj);

  document.querySelector('.js-button-number').innerHTML = `${lectures.length} ${getNoun(lectures.length, 'лекция', 'лекции', 'лекций')}`;
  getCarousel();

  /*Задание №2:
  1) Вам необходимо реализовать фильтры (img_04)
  2) По клику на фильтр, к примеру html, в списке "все курсы" должны будут отобразиться только курсы по html
  4) По клику на фильтр javascript должны выводиться курсы по javascript. (img_06)
  5) По клику на фильтр CSS должны выводиться курсы по CSS. (img_07)
  3) Способы реализации фильтрации вы выбираете сами "{{NUMBER}} лекций" должны выводиться все лекции. (img_01)*/

  document.querySelectorAll('.js-button-filter').forEach(function (filter) {
    filter.onclick = function () {
      document.querySelectorAll('.active-filter').forEach(function (act) {
        act.classList.remove('active-filter');
      });
      filter.classList.add('active-filter');

      document.querySelector(".js-all-lectures-items").querySelectorAll('.js-item').forEach(function (item) {
        item.classList.add('hidden');
        if (item.dataset.group == filter.dataset.group || filter.dataset.group == 'all') {
          item.classList.remove('hidden');
        }
      });
      document.querySelector(".js-all-lectures-items").style.transform = `translate(0px)`;
      getCarousel();
    };
  });
};

//функция получения url из полного пути картинки
function getUrlImg(string) {
  return string.substring(string.search('/img'));
}

//функция для вывода окончания слова "лекция" в зависимости от количества
function getNoun(number, one, two, five) {
  let n = Math.abs(number);
  n %= 100;
  if (n >= 5 && n <= 20) {
    return five;
  }
  n %= 10;
  if (n === 1) {
    return one;
  }
  if (n >= 2 && n <= 4) {
    return two;
  }
  return five;
}

//функция для расчета карусели лекций
function getCarousel() {
  const cardWidth = window.getComputedStyle(document.querySelector('.js-item'), null).getPropertyValue("width").slice(0, -2); // ширина карточки
  const cardMargin = window.getComputedStyle(document.querySelector('.js-item'), null).getPropertyValue("margin-right").slice(0, -2); //правый отступ
  const width = Number(cardWidth) + Number(cardMargin); // ширина карточки с правым отступом
  const count = 1; // шаг прокрутки (кол-во карточек)

  document.querySelectorAll('.js-container-quest').forEach(function (carousel) {
    let list = carousel.querySelector('.items');
    let leng = 0;
    carousel.querySelectorAll('.item').forEach(function (el) {
      if (!el.classList.contains('hidden')) {
        leng++;
      }
    });
    let position = 0; // положение ленты прокрутки

    carousel.querySelectorAll('.action').forEach(function (button) {
      button.onclick = function () {
        if (button.classList.contains('back')) {
          // сдвиг влево
          position += width * count;
          position = Math.min(position, 0);
        } else {
          // сдвиг вправо
          position -= width * count;
          position = Math.max(position, -width * (leng - count));
        };
        list.style.transform = `translate(${position}px)`;
      };
    });
  });
};
